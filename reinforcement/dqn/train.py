from dqn import DQN
import gym
from time import sleep
import os
import cv2

def main():
    env = gym.make("MountainCar-v0")
    trials = 100
    trial_len = 100
    state_size = env.observation_space.shape[0]

    dqn_agent = DQN(env=env)

    reward_history = 0

    for trial in range(trials):
        cur_state = env.reset().reshape(1,state_size)
        total_reward = 0
        for step in range(trial_len):
            action = dqn_agent.act(cur_state)
            new_state, reward, done, _ = env.step(action)

            new_state = new_state.reshape(1,state_size)
            dqn_agent.remember(cur_state, action, reward, new_state, done)

            dqn_agent.replay()

            total_reward += reward
            cur_state = new_state
            if done:
                if total_reward > reward_history:
                    dqn_agent.update_target()
                    dqn_agent.save_model('solved.h5')
                break

            if trial % 50 == 0:
                env.render()

        print('Trial = ', trial)
        print('Total Reward for Episode = ', total_reward)

        reward_history = total_reward

        if trial % 50 == 0:
            dqn_agent.save_model('backup.h5')

        env.close()



if __name__ == '__main__':
    main()
